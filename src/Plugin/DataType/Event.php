<?php

namespace Drupal\radar_connector\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;
use Drupal\radar_connector\TypedData\RadarEntityInterface;

/**
 * Radar Event Data.
 *
 * @DataType(
 *   id = "radar_event",
 *   label = @Translation("Event"),
 *   definition_class = "\Drupal\radar_connector\TypedData\EventDefinition"
 * )
 */
class Event extends Map implements RadarEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {
    parent::onChange($property_name, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function applyDefaultValue($notify = TRUE) {
    parent::applyDefaultValue($notify);
    $this->set('type', 'event', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    $array = parent::toArray();
    // Normalize behaviour, no point in returning -null- here.
    $array['language'] = $this->get('language')->id();
    foreach ($array as $key => $value) {
      if (empty($value)) {
        unset($array[$key]);
      }
    }
    return $array;
  }

}
