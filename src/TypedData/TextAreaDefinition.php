<?php

namespace Drupal\radar_connector\TypedData;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Longer text with format property definition.
 */
class TextAreaDefinition extends ComplexDataDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $this->propertyDefinitions['value'] = DataDefinition::create('string')
        ->setLabel('Text');
      $this->propertyDefinitions['summary'] = DataDefinition::create('string')
        ->setLabel('Summary')
        ->setDescription('Teaser or short summary if required.');
      $this->propertyDefinitions['format'] = DataDefinition::create('string')
        ->setLabel('Format');
    }
    return $this->propertyDefinitions;
  }

}
