<?php

namespace Drupal\radar_connector\TypedData;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Reference property definition.
 */
class ReferenceDefinition extends ComplexDataDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $this->propertyDefinitions['id'] = DataDefinition::create('string')
        ->setLabel('UUID')
        ->setDescription('Referenced entity\'s UUID. Required for push and pull.');

      $this->propertyDefinitions['uri'] = DataDefinition::create('uri')
        ->setLabel('URI')
        ->setDescription('Radar API endpoint for referenced entity.');
      $this->propertyDefinitions['resource'] = DataDefinition::create('string')
        ->setLabel('Type')
        ->setDescription('Referenced entity Radar type');
      $this->propertyDefinitions['name'] = DataDefinition::create('string')
        ->setLabel('Name');
    }
    return $this->propertyDefinitions;
  }

}
