<?php

namespace Drupal\radar_connector\TypedData;

/**
 * Typed data that can be pushed / pulled from Radar API.
 */
interface RadarEntityInterface {}
