<?php

namespace Drupal\radar_connector\TypedData;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Datetime property definition.
 */
class DateTimeDefinition extends ComplexDataDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $this->propertyDefinitions['value'] = DataDefinition::create('timestamp')
        ->setLabel('Start timestamp');
      $this->propertyDefinitions['value2'] = DataDefinition::create('timestamp')
        ->setLabel('End timestamp')
        ->setDescription('Same as start if no end time.');
      $this->propertyDefinitions['duration'] = DataDefinition::create('integer')
        ->setLabel('Duration');
      $this->propertyDefinitions['time_start'] = DataDefinition::create('datetime_iso8601')
        ->setLabel('Start time ISO format. Required for push');
      $this->propertyDefinitions['time_end'] = DataDefinition::create('datetime_iso8601')
        ->setLabel('Start time ISO format.');
      $this->propertyDefinitions['timezone'] = DataDefinition::create('string')
        ->setLabel('Timezone name. Required for push.')
        ->setDescription('IANA name, not offset.');
      $this->propertyDefinitions['rrule'] = DataDefinition::create('string')
        ->setLabel('iCal Repeat Rule.');
    }
    return $this->propertyDefinitions;
  }

}
