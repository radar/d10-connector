<?php

namespace Drupal\radar_connector\TypedData;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 * URL link property definition.
 */
class LinkDefinition extends ComplexDataDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {
      $this->propertyDefinitions['url'] = DataDefinition::create('uri')
        ->setLabel('URL');
      $this->propertyDefinitions['attributes'] = DataDefinition::create('string')
        ->setLabel('Attributes - @todo this is an array?');
      $this->propertyDefinitions['display_url'] = DataDefinition::create('string')
        ->setLabel('Display URL');
    }
    return $this->propertyDefinitions;
  }

}
