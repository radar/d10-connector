<?php

namespace Drupal\radar_connector\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;

/**
 * Radar File Reference property Data.
 *
 * @DataType(
 *   id = "radar_filereference",
 *   label = @Translation("File reference"),
 *   definition_class = "\Drupal\radar_connector\TypedData\FileReferenceDefinition"
 * )
 */
class FileReference extends Map {
}
