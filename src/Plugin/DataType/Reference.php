<?php

namespace Drupal\radar_connector\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;

/**
 * Radar Event Data.
 *
 * @DataType(
 *   id = "radar_reference",
 *   label = @Translation("Reference"),
 *   definition_class = "\Drupal\radar_connector\TypedData\ReferenceDefinition"
 * )
 */
class Reference extends Map {
}
