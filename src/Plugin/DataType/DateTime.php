<?php

namespace Drupal\radar_connector\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;

/**
 * Radar Date Time property Data.
 *
 * @DataType(
 *   id = "radar_datetime",
 *   label = @Translation("Date Time"),
 *   definition_class = "\Drupal\radar_connector\TypedData\ReferenceDefinition"
 * )
 */
class DateTime extends Map {
}
