<?php

namespace Drupal\Tests\radar_connector\Kernel;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Handler\MockHandler;
use Drupal\KernelTests\KernelTestBase;
use Drupal\radar_connector\RadarConnector;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Drupal\radar_connector\TypedData\EventDefinition;

/**
 * Basic Radar Connector functionality.
 *
 * @group radar_connector
 */
class ConnectorBasic extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['radar_connector'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installConfig('radar_connector');
    $this->typedDataManager = $this->container->get('typed_data_manager');
  }

  /**
   * Test login and post with authentication credentials.
   */
  public function testAuthentication() {
    \Drupal::configFactory()
      ->getEditable('radar_connector.client')
      ->set('username', 'test_user')
      ->set('password', 'test_pass')
      ->save(TRUE);

    $guzzleResponse = new MockHandler([
      function (RequestInterface $request) {
        return new Response(
          200,
          [
            'Set-Cookie' => new SetCookie([
              'Name'   => 'SESSid',
              'Value'  => 'session_key',
              'Domain' => '.radar.squat.net',
            ]),
          ],
          json_encode(['token' => '123Token'])
        );
      },
      new Response(200),
    ]);

    $container = [];
    $history = Middleware::history($container);
    $stack = HandlerStack::create($guzzleResponse);
    $stack->push($history);
    $client = new Client([
      'base_uri' => 'https://radar.squat.net/api/1.2/',
      'handler' => $stack,
    ]);

    $connector = new RadarConnector(NULL, $client);
    $connector->authenticate();
    $connector->request('POST', 'anywhere', TRUE, []);

    // Check sent credentials.
    $transaction = $container[0];
    $this->assertEquals('POST', $transaction['request']->getMethod());
    $this->assertEquals('https://radar.squat.net/api/1.2/user/login', $transaction['request']->getUri());
    $this->assertEqual(json_encode(['username' => 'test_user', 'password' => 'test_pass']), $transaction['request']->getBody()->getContents());

    // Check set authentication cookie and token used to post.
    $transaction = $container[1];
    $this->assertContains('123Token', $transaction['request']->getHeader('X-CSRF-Token'));
    $this->assertContains('SESSid=session_key', $transaction['request']->getHeader('Cookie'));
  }

  public function testAuthenticationFail() {
    // Check error handling.
  }

  public function testPush() {
    $event_definition = EventDefinition::create('radar_event');
    $event = $this->typedDataManager->create($event_definition);

    $event->set('title', 'Title');

    $guzzleResponse = new MockHandler([new Response(200, [])]);

    $container = [];
    $history = Middleware::history($container);
    $stack = HandlerStack::create($guzzleResponse);
    $stack->push($history);
    $client = new Client([
      'base_uri' => 'https://radar.squat.net/api/1.2/',
      'handler' => $stack,
    ]);

    $connector = new RadarConnector(NULL, $client);
    $connector->post('anywhere', $event);

    // Check sent credentials.
    $transaction = $container[0];
    $this->assertEquals('POST', $transaction['request']->getMethod());
    $contents = json_decode($transaction['request']->getBody()->getContents(), TRUE);
    // TODO all the others are NULL and posted as such. Desired, or undesired?
    $this->assertEquals('Title', $contents['title']);
  }

}
