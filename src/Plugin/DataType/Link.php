<?php

namespace Drupal\radar_connector\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;

/**
 * Radar Longtext property Data.
 *
 * @DataType(
 *   id = "radar_link",
 *   label = @Translation("URL link"),
 *   definition_class = "\Drupal\radar_connector\TypedData\LinkDefinition"
 * )
 */
class Link extends Map {
}
