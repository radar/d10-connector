<?php

namespace Drupal\radar_connector\TypedData;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Radar Event.
 *
 * Example data.
 *
 * Simple properties (useful for push):
 * "uuid": "1f0f8922-5b1c-4eca-81c9-32eb2da77d0e",
 * "vuuid": "7a4ffa58-0909-4123-bd9b-9e909723464c",
 * "type": "event",
 * "title": "Film",                 # non-translated
 * "title_field": "Film",           # translated @todo document
 * "language": "de",
 * "status": "1",
 * "created": "1529812874",
 * "changed": "1529812874",
 * "event_status": "confirmed",
 * "price": "freetext description of price" # see also price category
 * "phone": "020-1234567",
 * "email": "group@example.com",
 *
 * Textarea (only value required for push):
 * "body": {
 *   "value": "<p wrap=\"\">Body text</p>\n",
 *   "summary": "",
 *   "format": "rich_text_editor"
 * },
 *
 * Datetime (time_start, time_end and timezone required for push, rrule optional):
 * "date_time": [
 *   {
 *     "value": "1537722000",
 *     "value2": "1537722000",
 *     "duration": 0,
 *     "time_start": "2018-09-23T19:00:00+02:00",
 *     "time_end": "2018-09-23T19:00:00+02:00",
 *     "timezone": "Europe/Berlin",
 *     "rrule": "RRULE:FREQ=DAILY;INTERVAL=1;UNTIL=20181115T225959Z;WKST=MO"
 *   }
 * ],
 * Use the same time_end as time_start for an event with only a start time.
 *
 * References (only id required for push):
 * "category": [
 *   {
 *     "uri": "https://radar.squat.net/api/1.2/taxonomy_term/68197b93-2ece-4b0f-9a76-d9e99bda2603",
 *     "id": "68197b93-2ece-4b0f-9a76-d9e99bda2603",
 *     "resource": "taxonomy_term",
 *     "name": "course/workshop"
 *   }
 * ],
 * "topic": [
 *   {
 *     "uri": "https://radar.squat.net/api/1.2/taxonomy_term/22df4b12-ccbe-49c2-b47f-14536b13a4a7",
 *     "id": "22df4b12-ccbe-49c2-b47f-14536b13a4a7",
 *     "resource": "taxonomy_term",
 *     "name": "healing"
 *   }
 * ],
 * "price_category": [
 *   {
 *     "uri": "https://radar.squat.net/api/1.2/taxonomy_term/dfb542fc-f5b5-491d-87be-95f1c0813d57",
 *     "id": "dfb542fc-f5b5-491d-87be-95f1c0813d57",
 *     "resource": "taxonomy_term",
 *     "name": "free"
 *   }
 * ],
 * "og_group_ref": [
 *   {
 *     "uri": "https://radar.squat.net/api/1.2/node/9e43dac6-e1da-4f60-8428-de9f32ac9eb0",
 *     "id": "9e43dac6-e1da-4f60-8428-de9f32ac9eb0",
 *     "resource": "node"
 *   }
 * ],
 * "og_group_request": [
 *   {
 *     "uri": "https://radar.squat.net/api/1.2/node/2c517caa-f260-43c4-af4c-8380270a2425",
 *     "id": "2c517caa-f260-43c4-af4c-8380270a2425",
 *     "resource": "node"
 *   }
 * ],
 * "offline": [
 *   {
 *     "uri": "https://radar.squat.net/api/1.2/location/594c60d7-c61e-45c8-95bd-5e93b52509e5",
 *     "id": "594c60d7-c61e-45c8-95bd-5e93b52509e5",
 *     "resource": "location",
 *     "title": "ADM Hornweg 6  Amsterdam Netherlands"
 *   }
 * ],
 * "image": {
 *   "file": {
 *     "uri": "https://radar.squat.net/api/1.2/file/7acf1bd1-2b8d-47bd-8bc4-02b8ee2d1bb6",
 *     "id": "7acf1bd1-2b8d-47bd-8bc4-02b8ee2d1bb6",
 *     "resource": "file",
 *     "filename": "logo.png"
 *   }
 * },
 * "flyer": {
 *   "file": {
 *     "uri": "https://radar.squat.net/api/1.2/file/40e52d9e-b136-4c4e-8a9f-a45d50ffe6e8",
 *     "id": "40e52d9e-b136-4c4e-8a9f-a45d50ffe6e8",
 *     "resource": "file",
 *     "filename": "flyer_smb_05.pdf"
 *   },
 *   "description": ""
 * },
 *
 * External link fields:
 * "link": [
 *   {
 *     "url": "https://adm.amsterdam/event/sunday-voku-and-jam",
 *     "attributes": [],
 *     "display_url": null
 *   }
 * ],
 *
 * Radar properties that can be useful:
 * "url": "https://radar.squat.net/de/event/city/title",
 * "edit_url": "https://radar.squat.net/de/node/269670/edit",
 *
 * Additional simple properties (mainly internal, or unused, not for push):
 * "nid": "269670",
 * "vid": "287911",
 * "is_new": false,
 * "feed_nid": null,
 * "promote": "0",
 * "sticky": "0",
 */
class EventDefinition extends ComplexDataDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    if (!isset($this->propertyDefinitions)) {

      $this->propertyDefinitions['uuid'] = DataDefinition::create('string')
        ->setLabel('UUID');
      $this->propertyDefinitions['vuuid'] = DataDefinition::create('string')
        ->setLabel('Version UUID');
      $this->propertyDefinitions['type'] = DataDefinition::create('string')
        ->setLabel('Type: event');
      $this->propertyDefinitions['title'] = DataDefinition::create('string')
        ->setLabel('Title (non-translatable)');
      $this->propertyDefinitions['title_field'] = DataDefinition::create('string')
        ->setLabel('Title (translatable)');
      $this->propertyDefinitions['language'] = DataDefinition::create('language')
        ->setLabel('Language');
      $this->propertyDefinitions['status'] = DataDefinition::create('boolean')
        ->setLabel('Published status');
      $this->propertyDefinitions['created'] = DataDefinition::create('timestamp')
        ->setLabel('Created time')
        ->setDescription('Create time as a UTC timestamp.');
      $this->propertyDefinitions['changed'] = DataDefinition::create('timestamp')
        ->setLabel('Changed time')
        ->setDescription('Updated time as a UTC timestamp.');
      $this->propertyDefinitions['event_status'] = DataDefinition::create('string')
        ->setLabel('Event status')
        ->setDescription('iCal standard options: confirmed, tentative, cancelled.');
      $this->propertyDefinitions['price'] = DataDefinition::create('string')
        ->setLabel('Price')
        ->setDescription('Free text description of the price of the event.');
      $this->propertyDefinitions['phone'] = DataDefinition::create('string')
        ->setLabel('Phone');
      $this->propertyDefinitions['email'] = DataDefinition::create('email')
        ->setLabel('E-mail');

      $this->propertyDefinitions['body'] = DataDefinition::create('radar_textarea')
        ->setLabel('Body')
        ->setDescription('Main description of the event.');

      $this->propertyDefinitions['date_time'] = DataDefinition::create('radar_datetime')
        ->setLabel('Date')
        ->setDescription('Full event date, including any end time and timezone.');

      $this->propertyDefinitions['category'] = DataDefinition::create('radar_reference')
        ->setLabel('Category')
        ->setDescription('From a fixed set of Radar categories.');
      $this->propertyDefinitions['topic'] = DataDefinition::create('radar_reference')
        ->setLabel('Topic')
        ->setDescription('Tags, where additional can be created.');
      $this->propertyDefinitions['price_category'] = DataDefinition::create('radar_reference')
        ->setLabel('Price')
        ->setDescription('From a fixed set of Radar price descriptors.');
      $this->propertyDefinitions['og_group_ref'] = DataDefinition::create('radar_reference')
        ->setLabel('Groups the event is posted in.');
      $this->propertyDefinitions['og_group_request'] = DataDefinition::create('radar_reference')
        ->setLabel('Groups the event is proposed to.');
      $this->propertyDefinitions['offline'] = DataDefinition::create('radar_reference')
        ->setLabel('Location');
      $this->propertyDefinitions['image'] = DataDefinition::create('radar_filereference')
        ->setLabel('Image');
      $this->propertyDefinitions['flyer'] = DataDefinition::create('radar_filereference')
        ->setLabel('Flyer')
        ->setDescription('PDF or similar document.');

      $this->propertyDefinitions['link'] = DataDefinition::create('radar_link')
        ->setLabel('External URL');

      $this->propertyDefinitions['url'] = DataDefinition::create('uri')
        ->setLabel('URL')
        ->setDescription('Absolute URL to view event.');
      $this->propertyDefinitions['edit_url'] = DataDefinition::create('uri')
        ->setLabel('Edit URL')
        ->setDescription('Absolute URL of form to Edit event on Radar.');

      $this->propertyDefinitions['nid'] = DataDefinition::create('integer')
        ->setLabel('Radar Internal: Node ID');
      $this->propertyDefinitions['vid'] = DataDefinition::create('integer')
        ->setLabel('Radar Internal: Version ID');
      $this->propertyDefinitions['is_new'] = DataDefinition::create('boolean')
        ->setLabel('New entity not yet saved.');
      $this->propertyDefinitions['feed_nid'] = DataDefinition::create('integer')
        ->setLabel('Radar Internal: Feed Node ID');
      $this->propertyDefinitions['promote'] = DataDefinition::create('boolean')
        ->setLabel('Radar Internal: Promoted to front')
        ->setDescription('Presently unused.');
      $this->propertyDefinitions['sticky'] = DataDefinition::create('boolean')
        ->setLabel('Radar Internal: Stick at top of list')
        ->setDescription('Presently unused.');
    }
    return $this->propertyDefinitions;

  }

}
