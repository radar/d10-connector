<?php

namespace Drupal\Tests\radar_connector\Kernel;

use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Plugin\search_api\processor\AddURL;

use Drupal\radar_connector\TypedData\EventDefinition;
use Drupal\KernelTests\KernelTestBase;

/**
 *  Event Data Type. 
 *
 * @group radar_connector
 *
 * @see \Drupal\radar_connector\Plugin\DataType\Event
 */
class TestEventDataType extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['radar_connector'];

  protected function setUp() {
    parent::setup();
    $this->typedDataManager = $this->container->get('typed_data_manager');
  }

  function testLoadArray() {
    $event_json = file_get_contents(__DIR__ . '/../../fixtures/event.json');
    $event_array = json_decode($event_json, TRUE);
    $event_definition = EventDefinition::create('radar_event');
    $event = $this->typedDataManager->create($event_definition);
    $event->setValue($event_array);
    $this->assertEquals($event_array, $event->toArray());
  }
}
