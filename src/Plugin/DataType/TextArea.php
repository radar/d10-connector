<?php

namespace Drupal\radar_connector\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;

/**
 * Radar Longtext property Data.
 *
 * @DataType(
 *   id = "radar_textarea",
 *   label = @Translation("Textarea"),
 *   definition_class = "\Drupal\radar_connector\TypedData\TextAreaDefinition"
 * )
 */
class TextArea extends Map {
}
