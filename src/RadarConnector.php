<?php

namespace Drupal\radar_connector;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Cookie\CookieJar;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\radar_connector\TypedData\RadarEntityInterface;

/**
 * Basic connector.
 */
class RadarConnector {

  private $client;

  private $headers;

  private $cookies;

  public function __construct(ImmutableConfig $config = NULL, Client $client = NULL, CookieJar $cookies = NULL) {
    if (is_null($config)) {
      $config = \Drupal::config('radar_connector.client');
    }
    $this->config = $config;
    $this->headers = $this->config->get('headers') + ['Content-Type' => 'application/json', 'Accept' => 'application/json'];

    if (is_null($client)) {
      $client = new Client([
        'base_uri' => $this->config->get('api_endpoint'),
        'timeout' => $this->config->get('timeout'),
      ]);
    }
    $this->client = $client;

    if (is_null($cookies)) {
      $cookies = new CookieJar();
    }
    $this->cookies = $cookies;
  }

  /**
   * Authenticate.
   *
   * @throws GuzzleHttp\Exception\RequestException
   *   401 unauthorized login failure.
   */
  public function authenticate() {
    $response = $this->request(
      'POST',
      'user/login',
      TRUE,
      [
        'json' => [
          'username' => $this->config->get('username'),
          'password' => $this->config->get('password'),
        ],
      ]
    );

    $auth = json_decode($response->getBody());
    $this->headers['X-CSRF-Token'] = $auth->token;
  }

  /**
   * Raw Get.
   *
   * Generally GET requests don't need to be authenticated, and can retrieve
   * cached data from Radar.
   */
  public function get($path, $authenticated = FALSE, array $options = []) {
    return $this->request('GET', $path, $authenticated, $options);
  }

  /**
   * Post entity.
   */
  public function post($path, RadarEntityInterface $data, array $options = []) {
    $options += [
      'json' => $data->toArray(),
    ];

    return $this->request('POST', $path, TRUE, $options);
  }

  /**
   * Put entity.
   */
  public function put($path, RadarEntityInterface $data, array $options = []) {
    $options += [
      'json' => $data->toArray(),
    ];

    return $this->request('PUT', $path, TRUE, $options);
  }

  /**
   * Guzzle Client::request wrapped for logging, and authentication.
   */
  public function request($method, $path, $authenticated = FALSE, array $options = []) {
    if ($authenticated) {
      $options += [
        'cookies' => $this->cookies,
        'headers' => $this->headers,
      ];
    }

    try {
      $response = $this->client->request($method, $path, $options);
    }
    catch (RequestException $e) {
      // @todo logging.
      throw $e;
    }

    return $response;
  }

}
